This script allows you to evaluate expressions in a repl with frame context

To use, when a debugger prompt is active, click on a frame and press Z.


You do not need this library to perform this functionality. In standard sly if you press E it will use the minibuffer as the prompt with frame context (sly-db-eval-in-frame). This library is meant for people who do not want to use the minibuffer for this functionality.

here is some example code to try this with:
```
(step
 (let ((some-local 12345))
   (print "Let's use the debugger to find out what (gcd some-local 54321) is")
   (setq *dummy* some-local)))
```

When executing this, move cursor to Frame 0: in the Backtrace: section and press Z.
it should drop you into a new, full blown REPL. It will print all the locals available to you and they are available to use in expressions. You can switch between frames with Z as well.

Now at the bottom you can type (gcd some-local 54321) RET - some-local will be available just like when pressing E. Your original REPL will be blocked waiting for the step form to return as you type away in this repl (which is why I couldn't just use it instead).

here is an example, bottom left is the sly mrepl db instance, bottom right is the original repl that invoked the step form condition.

![link](example.png)

this code is mostly copy pasted mishmash from sly.el and sly-mrepl.el.

I'm unfamiliar with emacs modes and such but I have tried this for several days and surprisingly haven't had any issues so far. I got most buttons working even. The return value button currently might not work if it returns something non-atomic like a closure or object etc. but the rest of the buttons should. I might fix this later.


Load with `(load "sly-mrepl-sly-db.el")`
